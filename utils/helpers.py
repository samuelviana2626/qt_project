from unittest import TestCase
from models.connection import connect

send = connect()


class Utils:
    @staticmethod
    def save_employee(name, salary, gender, birth):
        try:
            conn = send.conn
            curs = send.curs
            result = birth.split("/")
            result = '-'.join(reversed(result))
            curs.execute('insert into employee(name,salary,gender, birth) values(%s, %s, %s, %s)',
                         (name, salary, gender, result))
            conn.commit()
            return "inseriu"
        except Exception:
            return "não inseriu"

    @staticmethod
    def list_employeer():
        try:
            conn = send.conn
            curs = conn.cursor()
            curs.execute('select * from employee')
            result = curs.fetchall()
            return result
        except Exception:
            return "não retornou"

    @staticmethod
    def get_db():
        method = Utils()
        request_db = method.list_employeer()
        list = []
        for elem in request_db:
            list.append({
                'id': elem[0],
                'name': elem[1],
                'salary': elem[2],
                'gender': elem[3],
                'birthdate': elem[4]
            })

        return list

    @staticmethod
    def on_edit_click():
        try:
            send.curs.execute('delete from employee where id = %s', str(1))
            return "deletou"
        except Exception as erro:
            return f"{erro}"


# result = Utils.get_db()
# print(len(result))
# for index, value in enumerate(result):
#     print(value.get("name"))
test = Utils()
# result = test.list_employeer()
#
class InserTest(TestCase):
    def test_delete(self):
        result = test.on_edit_click()
        self.assertEqual(result, "deletou")
#     def setUp(self) -> None:
#         self.name = "samuel"
#         self.salary = 1500
#         self.gender = "M"
#         self.birth = "1997-10-26"
#
#     def test_insert(self):
#         returno = test.save_employee(self.name, self.salary, self.gender, self.birth)
#         self.assertEqual(returno, "inseriu")
#
#     def test_retorno(self):
#         result = test.list_employeer()
#         self.assertEqual(result, "retornou")
