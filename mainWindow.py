import sys
import re
from PyQt5.QtCore import QDateTime, QRegExp
from PyQt5.QtGui import QDoubleValidator, QRegExpValidator, QIcon, QPixmap
import locale

from views.modal_form import Modal_Form

from models.user import Employee
from utils.helpers import Utils
from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QVBoxLayout, QWidget, QLineEdit, QHBoxLayout, \
    QSpacerItem, QSizePolicy, QComboBox, QPushButton, QTableWidget, QTableWidgetItem, QMessageBox, \
    QDateEdit, QHeaderView

from models.connection import connect

send = connect()
method = Utils


class MainForm(QMainWindow):
    def __init__(self,parent=None):
        super().__init__(parent=parent)
        self.setWindowTitle("first application")
        self.setGeometry(20, 20, 800, 400)

        rxy = QRegExp("[A-Za-z ]+")
        validator_name = QRegExpValidator(rxy)

        self.label_nome = QLabel()
        self.label_nome.setText("Name")
        self.edt_name = QLineEdit()
        self.edt_name.setValidator(validator_name)

        rx = QRegExp("([0-9]*[.])?[0-9]+")
        validator_salary = QRegExpValidator(rx)

        self.label_salary = QLabel()
        self.label_salary.setText("Salary")
        self.edt_salary = QLineEdit()
        self.edt_salary.setValidator(validator_salary)

        self.label_gender = QLabel()
        self.label_gender.setText("Gender")
        self.edt_gender = QComboBox()
        self.edt_gender.addItem("M")
        self.edt_gender.addItem("F")
        self.edt_gender.addItem("O")

        self.label_birth = QLabel()
        self.label_birth.setText("Birth")
        self.dateedit = QDateEdit(calendarPopup=True)
        self.menuBar().setCornerWidget(self.dateedit)
        self.dateedit.setDateTime(QDateTime.currentDateTime())

        self.push_button = QPushButton("Send")
        self.push_button.clicked.connect(self.send_button)

        space = QSpacerItem(0, 0, QSizePolicy.Fixed, QSizePolicy.Expanding)

        self.top = QHBoxLayout()
        self.top.addWidget(self.label_nome)
        self.top.addWidget(self.edt_name)
        self.top.addWidget(self.label_gender)
        self.top.addWidget(self.edt_gender)

        self.down = QHBoxLayout()
        self.down.addWidget(self.label_salary)
        self.down.addWidget(self.edt_salary)
        self.down.addWidget(self.label_birth)
        self.down.addWidget(self.dateedit)

        self.vertical = QVBoxLayout()

        self.table = QTableWidget()
        self.table.setColumnCount(6)
        self.table.setHorizontalHeaderLabels(["Name", "Salary", "Gender", "Birthdate", "Delete", "Update"])
        self.table.horizontalHeader().setStretchLastSection(True)
        self.table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.table.setRowCount(0)

        self.vertical.addLayout(self.top)
        self.vertical.addLayout(self.down)
        self.vertical.addWidget(self.push_button)
        self.vertical.addWidget(self.table)
        self.vertical.addItem(space)

        self.panel = QWidget()
        self.panel.setLayout(self.vertical)
        self.setCentralWidget(self.panel)

        self.populate()

    def send_button(self):
        locale.setlocale(locale.LC_ALL, )
        text_name = re.sub(' +', ' ', self.edt_name.text())
        text_gender = self.edt_gender.currentText()
        text_salary = self.edt_salary.text()
        text_birth = self.dateedit.text()

        try:
            if text_name.lstrip() == "" or text_salary == "" or text_birth == "":
                self.dialog("there are inputs to fill")
                #


            else:
                locale.setlocale(locale.LC_ALL, )
                emp = Employee(text_name, text_gender, text_salary, text_birth)
                method.save_employee(emp.name, emp.salary, emp.gender, emp.birth)
                self.edt_name.clear()
                self.edt_salary.clear()

                lenght = self.table.rowCount()
                self.table.insertRow(lenght)

                nome = QTableWidgetItem()
                nome.setText(emp.name)

                salary = QTableWidgetItem()
                salary.setText(locale.currency(float(emp.salary), grouping=True))

                gender = QTableWidgetItem()
                gender.setTextAlignment(4)
                gender.setText(emp.gender)

                birth = QTableWidgetItem()
                birth.setText(str(emp.birth))

                delete = self.create_table_button("Delete")
                update = self.updade_table_db("Update")

                self.table.setItem(lenght, 0, nome)
                self.table.setItem(lenght, 1, salary)
                self.table.setItem(lenght, 2, gender)
                self.table.setItem(lenght, 3, birth)
                self.table.setCellWidget(lenght, 4, delete)
                self.table.setCellWidget(lenght, 5, update)

                self.dialog("congratulations")

        except Exception as erro:
            self.dialog(erro)

            # python = sys.executable
            # os.execl(python, python, *sys.argv)

    def create_table_button(self, text_button):
        button = QPushButton()
        button.setText(text_button)
        button.setProperty('type', 'normal')
        return button

    def updade_table_db(self, text_button):
        button = QPushButton()
        button.setText(text_button)
        button.setProperty('type', 'normal')
        return button

    def populate(self):
        locale.setlocale(locale.LC_ALL, )
        employee = method.get_db()
        self.table.setRowCount(len(employee))
        for linha, valor in enumerate(employee):
            nome = QTableWidgetItem()
            nome.setText(valor.get("name"))

            salary = QTableWidgetItem()
            salary.setText(locale.currency(valor.get("salary"), grouping=True))

            gender = QTableWidgetItem()
            gender.setTextAlignment(4)
            gender.setText(valor.get("gender"))

            birth = QTableWidgetItem()
            aux = str(valor.get("birthdate")).split('-')
            birthdate = '/'.join(reversed(aux))
            birth.setText(birthdate)

            button = self.create_table_button("Delete")
            button.clicked.connect(
                lambda clicked, data={
                    'index': linha,
                    'pk': valor.get("id")
                }: self.on_edit_click(data)
            )
            update = self.updade_table_db("Update")
            update.clicked.connect(
                lambda clicked, data={
                    'index': linha,
                    'pk': valor.get("id"),
                    'name': valor.get("name"),
                    'salary': valor.get("salary"),
                    'gender': valor.get("gender"),
                    'birth': valor.get("birthdate")
                }: self.on_edit(data)
            )

            self.table.setItem(linha, 0, nome)
            self.table.setItem(linha, 1, salary)
            self.table.setItem(linha, 2, gender)
            self.table.setItem(linha, 3, birth)
            self.table.setCellWidget(linha, 4, button)
            self.table.setCellWidget(linha, 5, update)

    def on_edit_click(self, data: dict):
        pk = data.get("pk")
        try:
            send.curs.execute(f'delete from employee where id = {pk}')
            send.conn.commit()
            self.dialog('Deleted success')
        except Exception as erro:
            self.dialog(f'Error\n{erro}')

    def on_edit(self, data: dict):
        self.dialog = Modal_Form(self, **data)
        self.dialog.exec_()


    def dialog(self, text, title="Message"):
        dlg = QMessageBox(self)
        dlg.setWindowTitle(title)
        dlg.setText(text)
        button = dlg.exec()
        if button == QMessageBox.Ok:
            pass


app = QApplication(sys.argv)
main = MainForm()
main.show()
sys.exit(app.exec_())
