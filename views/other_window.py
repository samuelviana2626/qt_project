from random import randint

from PyQt5.QtWidgets import QWidget, QVBoxLayout, QLabel, QLineEdit, QPushButton


class AnotherWindow(QWidget):
    """
    This "window" is a QWidget. If it has no parent, it
    will appear as a free-floating window as we want.
    """

    def __init__(self):
        super().__init__()
        layout = QVBoxLayout()
        self.label_name = QLabel("Name")
        self.edt_modal_name = QLineEdit()

        self.label_salary = QLabel("Salary")
        self.edt_modal_salary = QLineEdit()

        self.update_button = QPushButton("Update")

        layout.addWidget(self.label_name)
        layout.addWidget(self.edt_modal_name)

        layout.addWidget(self.label_salary)
        layout.addWidget(self.edt_modal_salary)

        layout.addWidget(self.update_button)
        self.setLayout(layout)
