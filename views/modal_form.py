import sys
import re
from PyQt5.QtCore import QDateTime, QRegExp
from PyQt5.QtGui import QDoubleValidator, QRegExpValidator, QIcon, QPixmap
import locale

from models.user import Employee
from utils.helpers import Utils
from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QVBoxLayout, QWidget, QLineEdit, QHBoxLayout, \
    QSpacerItem, QSizePolicy, QComboBox, QPushButton, QTableWidget, QTableWidgetItem, QInputDialog, QMessageBox, \
    QDateEdit, QHeaderView, QDialog

from models.connection import connect

send = connect()
method = Utils

from PyQt5.QtGui import QDoubleValidator
from PyQt5.QtWidgets import QDialog, QWidget, QVBoxLayout, QLabel, QLineEdit, QHBoxLayout, QHeaderView, \
    QTableWidget, \
    QComboBox, QSpacerItem, QSizePolicy, QPushButton, QMainWindow


class Modal_Form(QDialog):

    def __init__(self, parent=None, **kwargs):
        super().__init__(parent=parent)
        name = kwargs.get("name")

        self.setWindowTitle("Form Update")
        self.setGeometry(10, 10, 400, 200)

        space = QSpacerItem(0, 0, QSizePolicy.Fixed, QSizePolicy.Expanding)

        self.label_name = QLabel()
        self.label_name.setText("Name")
        self.edt_name = QLineEdit(name)

        salary = kwargs.get("salary")
        self.label_salary = QLabel()
        self.label_salary.setText("Salary")
        self.edt_salary = QLineEdit(str(salary))
        self.edt_salary.setValidator(QDoubleValidator())

        self.label_gender = QLabel()
        self.label_gender.setText("Gender")
        self.edt_gender = QComboBox()
        self.edt_gender.addItems(["Male", "Female"])

        birth = kwargs.get("birthdate")
        self.label_birthday = QLabel()
        self.label_birthday.setText("Birthday")
        self.edt_birthday = QLineEdit(birth)

        self.layout = QVBoxLayout()
        self.top = QHBoxLayout()
        self.down = QHBoxLayout()

        self.top.addWidget(self.label_name)
        self.top.addWidget(self.edt_name)
        self.top.addWidget(self.label_gender)
        self.top.addWidget(self.edt_gender)

        self.down.addWidget(self.label_salary)
        self.down.addWidget(self.edt_salary)
        self.down.addWidget(self.label_birthday)
        self.down.addWidget(self.edt_birthday)

        self.layout.addLayout(self.top)
        self.layout.addLayout(self.down)

        self.layout.addItem(space)

        self.push_button = QPushButton("Update")
        self.layout.addWidget(self.push_button)

        self.setLayout(self.layout)
